package cards

import cards.types.{Action, Card}
import game.Player

case class Artisan() extends Card("Artisan",6,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.ArtisanAction()
  }
}
