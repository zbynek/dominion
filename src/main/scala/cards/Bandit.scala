package cards

import cards.types.{Action, Attack, Card}
import game.Player

case class Bandit() extends Card("Bandit",5,action = true,attack = true) with Action with Attack{
  override def Action(implicit player:Player) = {
    player.gainCard(Gold())
    Attack
  }

  def Attack(implicit player: Player): Unit = {
      player.getSession().players.tail.foreach(x=>x.BanditAttack())

  }


}
