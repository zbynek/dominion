package cards

import cards.types.{Action, Attack, Card}
import game.{Player, Session}

case class Bureaucrat() extends Card("Bureaucrat",4,action = true,attack = true) with Action with Attack{
  override def Action(implicit player:Player) = {
    player.gainCard(Silver())
    Attack
  }

  def Attack(implicit player:Player)={
   val ss = player.getSession()
    ss.players.tail.map({
      p => p.BureucratAttack()
    })
  }
}
