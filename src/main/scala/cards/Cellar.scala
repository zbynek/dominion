package cards

import cards.types.{Action, Card}
import game.Player

case class Cellar() extends Card("Cellar",2,action = true) with Action{

  override def Action(implicit player:Player) = {
    player.actions += 1
    val numberOfDiscarded = player.discardAnyNumber()
    player.drawXcards(numberOfDiscarded)
  }
}
