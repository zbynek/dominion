package cards

import cards.types.{Action, Card}
import game.Player

case class Chapel() extends Card("Chapel",2,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.trashHandUpTo(4)
  }
}
