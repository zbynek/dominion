package cards

import cards.types.{Card, Treasure}
import game.Player

case class Copper() extends Card("Copper",0,basic = true,gain = 1,treasure = true) with Treasure{

}
