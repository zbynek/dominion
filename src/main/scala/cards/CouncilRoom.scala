package cards

import cards.types.{Action, Card}
import game.Player

case class CouncilRoom() extends Card("Council Room",5,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.drawXcards(4)
    player.buys += 1
    player.getSession().players.tail.foreach(_.drawXcards(1))

  }
}
