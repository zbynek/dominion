package cards

import cards.types.Card
import game.Player

case class Curse() extends Card("Curse",0,basic = true,curse = true,victoryPoints = -1) with cards.types.Curse {

}
