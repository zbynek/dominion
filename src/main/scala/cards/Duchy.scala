package cards

import cards.types.{Card, Victory}
import game.Player

case class Duchy() extends Card("Duchy",5,basic = true,victory = true,victoryPoints = 3) with Victory{

}
