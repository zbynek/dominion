package cards

import cards.types.{Card, Victory}
import game.Player

case class Estate() extends Card("Estate",2,basic = true,victory = true,victoryPoints = 1) with Victory{

}
