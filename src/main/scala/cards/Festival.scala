package cards

import cards.types.{Action, Card}
import game.Player

case class Festival() extends Card("Festival",5,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.actions += 2
    player.buys += 1
    player.money += 2
  }
}
