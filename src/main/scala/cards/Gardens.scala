package cards

import cards.types.{Card, Victory}
import game.Player

case class Gardens() extends Card("Gardens",4,victory = true) with Victory{

  override def getVP(implicit player: Player):Int={
    player.deck.length / 10
  }
}
