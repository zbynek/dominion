package cards

import cards.types.{Card, Treasure}
import game.Player

case class Gold() extends Card("Gold",6,basic = true,gain = 3,treasure = true) with Treasure{

}
