package cards

import cards.types.{Action, Card}
import game.Player

case class Harbinger() extends Card("Harbinger",3,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.drawXcards(1)
    player.actions += 1
    player.lookAndMayPlaceXcardsFromDiscardPileontoDeck(1)
  }
}
