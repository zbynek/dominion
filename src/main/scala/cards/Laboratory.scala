package cards

import cards.types.{Action, Card}
import game.Player

case class Laboratory() extends Card("Laboratory",5,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.drawXcards(2)
    player.actions += 1
  }
}
