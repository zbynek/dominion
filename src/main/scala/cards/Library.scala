package cards

import cards.types.{Action, Card}
import game.Player

import scala.collection.mutable.ListBuffer

case class Library() extends Card("Library",5,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.LibraryAction()
  }
}
