package cards

import cards.types.{Action, Card}
import game.Player

case class Market() extends Card("Market",5,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.drawXcards(1)
    player.actions += 1
    player.buys += 1
    player.money += 1
  }
}
