package cards

import cards.types.{Action, Card}
import game.Player

case class Merchant(var firstCharged:Boolean=true) extends Card("Merchant",3,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.drawXcards(1)
    player.actions += 1
    /*
    player.effects += (x => {
      var charged = true
      if(x.getName=="Silver" && charged){
        charged = false
        player.money += 1
      }
    })
    */
    player.effects += (x => {
      if(x.getName=="Silver" ){
        player.money += 1
        true
      }else{
        false
      }
    })
  }




}
