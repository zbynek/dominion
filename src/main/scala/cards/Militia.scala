package cards

import cards.types.{Action, Attack, Card}
import game.Player

case class Militia() extends Card("Militia",4,action = true,attack = true) with Action with Attack{
  override def Action(implicit player:Player) = {
    player.money += 2
    Attack
  }

  def Attack(implicit player:Player): Unit = {
    player.getSession().players.tail.foreach(_.MilitiaAttack())
  }


}
