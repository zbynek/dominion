package cards

import cards.types.{Action, Card, Treasure}
import game.Player

case class Mine() extends Card("Mine",5,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.MineAction()
  }
}
