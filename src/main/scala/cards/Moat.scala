package cards

import cards.types.{Action, Card, Reaction}
import game.Player

case class Moat() extends Card("Moat",2,action = true,reaction = true) with Action with Reaction{
  override def Action(implicit player:Player) = {
    player.drawXcards(2)
  }

  override def Reaction: Unit = {
    println("revealing this card" + this.getName)
        //reveal this card and thats it
  }
}
