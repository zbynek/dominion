package cards

import cards.types.{Action, Card}
import game.Player

case class Moneylender() extends Card("Moneylender",4,action = true) with Action{
  override def Action(implicit player:Player) = {
    if(player.mayTrashFromHand(Copper())){
      player.money += 3
    }
  }
}
