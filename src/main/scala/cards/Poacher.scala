package cards

import cards.types.{Action, Card}
import game.Player

case class Poacher() extends Card("Poacher",4,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.drawXcards(1)
    player.actions += 1
    player.money += 1
    player.discard(player.getSession().emptyPile())
  }
}
