package cards

import cards.types.{Card, Victory}


case class Province() extends Card("Province",8,basic = true,gameEnding = true,victory = true,victoryPoints = 6) with Victory{

}
