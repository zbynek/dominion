package cards

import cards.types.{Action, Card}
import game.Player

case class Remodel() extends Card("Remodel",4,action = true) with Action{
  override def Action(implicit player:Player) = {
    val trashedCard = player.trashOneFromHand()
    if(trashedCard.isDefined){
      player.gainCardUpTo(trashedCard.get.getPrice+2)
    }
  }
}
