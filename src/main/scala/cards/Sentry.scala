package cards

import cards.types.{Action, Card}
import game.Player

case class Sentry() extends Card("Sentry",5,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.SentryAction()
  }
}
