package cards

import cards.types.{Card, Treasure}
import game.Player

case class Silver() extends Card("Silver",3,basic = true,gain = 2,treasure = true) with Treasure{

}
