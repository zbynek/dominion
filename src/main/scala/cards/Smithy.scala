package cards

import cards.types.{Action, Card}
import game.Player

case class Smithy() extends Card("Smithy",4,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.drawXcards(3)
  }
}
