package cards

import cards.types.{Action, Card}
import game.Player

case class ThroneRoom() extends Card("Throne Room",4,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.ThroneRoomAction()

  }
}
