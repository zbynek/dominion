package cards

import cards.types.{Action, Card}
import game.Player

case class Vassal() extends Card("Vassal",3,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.money += 2
    val discardedCard = player.discardTopDeck()
    if(discardedCard.isAction){
        if(player.mayPlayAsAction(discardedCard)){
          player.discardPile -= discardedCard
          player.played += discardedCard
        }
    }
  }


}
