package cards

import cards.types.{Action, Attack, Card}
import game.Player

case class Witch() extends Card("Witch",5,action = true,attack = true) with Action with Attack{
  override def Action(implicit player:Player) = {
    player.drawXcards(2)
    Attack
  }

  def Attack(implicit player: Player): Unit = {
    player.getSession().players.tail.foreach({ p=>
      p.WitchAttack()
    })
  }


}
