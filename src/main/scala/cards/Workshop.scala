package cards

import cards.types.{Action, Card}
import game.Player

case class Workshop() extends Card("Workshop",3,action = true) with Action{
  override def Action(implicit player:Player) = {
    player.gainCardUpTo(4)
  }
}
