package cards.types

import cards._
import game.Player

class Card (
           name:String,
           price:Int,
           basic:Boolean = false, // is always in the game setup
           gameEnding:Boolean = false, // if you cannot buy it, the game ends
           action:Boolean = false,
           attack:Boolean = false,
           curse:Boolean = false,
           reaction:Boolean = false,
           treasure:Boolean = false,
           victory:Boolean = false,
           gain:Int = 0,
           victoryPoints:Int = 0
           ){

  def isGameEnding:Boolean=gameEnding
  def isAction:Boolean=action
  def isBasic:Boolean=basic
  def isAttack:Boolean=attack
  def isCurse:Boolean=curse
  def isReaction:Boolean=reaction
  def isTreasure:Boolean=treasure
  def isVictory:Boolean=victory
  def getPrice:Int=price
  def getName:String={
    if(reaction){
      Console.BLUE + Console.BOLD + name + Console.RESET
    } else if(victory){
      Console.GREEN + Console.BOLD + name + Console.RESET
    }else if(treasure){
      Console.YELLOW + Console.BOLD + name + Console.RESET
    }else if(action){
      Console.WHITE + Console.BOLD + name + Console.RESET
    }else{
      name
    }
  }
  def getGain:Int=gain

  override def toString: String = getName

  def trash: Unit ={

  }

  def lastingEffect:Unit={

  }

  def play(implicit player: Player): Unit ={

  }

  def detail:String={
    name
  }

  def Action(implicit player: Player): Unit ={

  }

  def Reaction={

  }

  def getVP(implicit player: Player):Int={
    victoryPoints
  }


}

object Card{
  def getAllCards:List[Card]={
    List(Artisan(),Bandit(),Bureaucrat(),Cellar(),Chapel(),Copper(),CouncilRoom(),Curse(),Duchy(),
      Estate(),Festival(),Gardens(),Gold(),Harbinger(),Laboratory(),Library(),Market(),Merchant(),
      Militia(),Mine(),Moat(),Moneylender(),Poacher(),Province(),Remodel(),Sentry(),Silver(),Smithy(),
      ThroneRoom(),Vassal(),Village(),Witch(),Workshop())
  }
}
