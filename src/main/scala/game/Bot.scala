package game

import cards.types.Card

import scala.collection.mutable.ListBuffer

class Bot(name:String, deck:ListBuffer[Card],session: Session) extends PlayerCleaner(name,deck,session) {

/*
  override def decide(options:List[Card],text:String,forced:Boolean):Option[Card]={
    session.endTheGameCheck()
    while(true){
      println("HAND> "+hand.map(_.getName+" ").mkString)
      println(text)
      if(!forced)println("(0) do nothing")
      for(x <- 1 to options.length){
        println("("+x+") "+options(x-1).getName)
      }
      try {
        // INFO XXXXXXXXXXXXXXXXX logika :D XXXXXXXXXXXXXXXXXXXXX

        val index = logic(options,text,forced)

        // INFO XXXXXXXXXXXXXXXXX logika :D XXXXXXXXXXXXXXXXXXXXX

        index match {
          case 0 => if(forced){println("Not a valid option")}else return None
          case index if 1 until (options.length+1) contains index =>
            println(options(index-1) + " selected")
            return Option(options(index-1))
          case _ => println("Not a valid option")
        }
      }catch {
        case e:Exception => println("Not a number")
      }
    }
    None
  }

  def logic(options:List[Card],text:String,forced:Boolean): Int ={
    if(forced){
      scala.util.Random.nextInt(options.length)+1
    }else{
      scala.util.Random.nextInt(options.length+1)
    }
  }
  */
  override def decide(options:List[Card],text:String,forced:Boolean):Option[Card]={
    //session.endTheGameCheck()
    while(true){
      println("HAND> "+hand.map(_.getName+" ").mkString)
      println(text)
      if(!forced)println("(0) do nothing")
      for(x <- 1 to options.length){
        println("("+x+") "+options(x-1).getName)
      }
      try {
        val card:Option[Card] = logic(options,text,forced)
        card match {
          case None => if(forced){println("Not a valid option")}else return None
          case Some(value) if options.contains(value) =>
            println(card.get.getName + " selected")
            return card
          case _ => println("Not a valid option")
        }
      }catch {
        case e:Exception => println(e)
      }
    }
    None
  }

  def logic(options:List[Card],text:String,forced:Boolean): Option[Card] ={
    val index = if(forced){
      scala.util.Random.nextInt(options.length)+1
    }else{
      scala.util.Random.nextInt(options.length+1)
    }
    (None :: options.map(c => Option(c)))(index)
  }

}
