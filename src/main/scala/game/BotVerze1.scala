package game

import cards.types.Card

import scala.collection.mutable.ListBuffer

class BotVerze1(name:String, deck:ListBuffer[Card], session: Session) extends Bot(name,deck,session) {


  override def logic(options:List[Card],text:String,forced:Boolean): Option[Card] ={

    text match {
      case "you may plan an action" => options.sortBy(_.getPrice).lastOption
      case "you may play a treasure" => options.lastOption
      case "you may buy a card" => buy(options,forced)
      //case "you may use any of these reaction cards" =>
      //case "reveal victory card and put it onto deck" =>
      case "trash a revealed treasure other than copper and discard the rest" => options.sortBy(_.getGain).headOption
      //case "put a card from your hand onto your deck." =>
      case "you can set card aside instead, discarding it afterwards" => if(this.actions>0){None}else{options.lastOption}
      case "you may trash a card to gain a treasure costing up to 3 more than it" => options.filterNot(_.getName=="Gold").sortBy(_.getPrice).lastOption
      //case text if text.startsWith("you can TRASH or DISCARD the following card? Otherwise put it back on top of your deck:")=>
      case "you may play an action card from your hand twice" => options.sortBy(_.getPrice).lastOption
      case "trash a card from your hand to gain a card costing up to 2 mode than it" => options.sortBy(_.getPrice).headOption
      case "you may trash a card from your hand for +3" => options.filter(c=> c.isCurse || c.getName=="Estate" || c.getName=="Copper").sortBy(_.getVP(this)).headOption
      case text if text.startsWith("gain a card costing up to") => options.filter(_.isTreasure).sortBy(_.getPrice).lastOption
      case "You may discard any number of cards" => None
      case "discard a card" => options.sortBy(_.getPrice).headOption
      case text if text.startsWith("discard down to ")  => options.sortBy(_.getPrice).headOption
      case "you may trash a card (4 in total)" => options.find(c=> c.isCurse || c.getName=="Estate")
      case "you may put a card from your discard pile onto your deck" => options.filter(_.isTreasure).sortBy(_.getGain).lastOption
      case _ => random(options,forced)
    }

  }

  def random(options:List[Card], forced:Boolean): Option[Card] ={
    val index = if(forced){
      scala.util.Random.nextInt(options.length)+1
    }else{
      scala.util.Random.nextInt(options.length+1)
    }
    (None :: options.map(c => Option(c)))(index)
  }

  def buy(options:List[Card], forced:Boolean): Option[Card]={
    if((getAllCards().size / getAllCards().map(_.getGain).sum) >= 1.63){
      options.sortBy(_.getGain).lastOption
    }else if(options.exists(_.isAction) && (getAllCards().count(_.isAction) / getAllCards().size <= 0.2)){
      options.sortBy(_.getPrice).lastOption
    }else{
      options.filter(c => c.isVictory && !c.isCurse).sortBy(_.getPrice).lastOption
    }
  }

}
