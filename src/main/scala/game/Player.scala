package game

import cards.Curse
import cards.types.Card
import scala.collection.mutable.ListBuffer

class Player(name:String, _deck:ListBuffer[Card],session: Session
                 ) {
  var deck:ListBuffer[Card] = _deck
  var hand:ListBuffer[Card] = ListBuffer()
  var played:ListBuffer[Card] = ListBuffer()
  var discardPile:ListBuffer[Card] = ListBuffer()
  var trash:ListBuffer[Card] = ListBuffer()
  var effects:ListBuffer[Card=>Boolean] = ListBuffer()
  var actions:Int = 0
  var money:Int = 0
  var buys:Int = 0
  var turn:Int = 0

  def getAllCards():List[Card]={
    val list = ListBuffer[Card]()
    list ++= deck
    list ++= hand
    list ++= played
    list ++= discardPile
    list.toList
  }

  def getDeck()=deck

  def getName()=name

  def cleanUp(): Unit ={
    actions = 1
    money = 0
    buys = 1
    discardPile ++= played
    discardPile ++= hand // neni specifikovano co jde na discard pile driv
    played.clear()
    hand.clear()
    drawXcards(5)
  }

  def reaction():Boolean={
    if(hand.exists(_.isReaction)){
      println("You may use any of these reaction cards")
      hand.filter(_.isReaction).foreach(x => println(x))
      val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
      val optCard = hand.filter(_.isReaction).find(x => x.getName == name)
      if(optCard.isDefined){
        optCard.get.Reaction
        return false
      }
    }
    true
  }


  def BureucratAttack()={
    if(reaction()){
      println("reveal victory and put it onto deck")
      val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
      val optCard = hand.find(x => x.getName == name)
      if(optCard.isDefined){
        deck += optCard.get
        hand -= optCard.get
      }else{
        println("revealed cards" + hand)
      }
    }
  }

  def BanditAttack(): Unit ={
    if(reaction()){
      var reveal = ListBuffer[Card]()
      for(y <- 1 to 2){
        if(deck.isEmpty){
          discardIntoDeck
          shuffleDeck
        }
        if(deck.nonEmpty){
          reveal += deck.head
          deck -= deck.head
        }
      }
      if(reveal.exists(x => x.isTreasure && x.getName!="Copper")){
        println("trash a revealed treasure other than copper and discard the rest")
        val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
        val optCard = reveal.find(x => x.getName == name)
        if(optCard.isDefined){
          reveal -= optCard.get
          trash += optCard.get
        }
        discardPile ++= reveal
      }
    }
  }

  def MilitiaAttack(): Unit ={
    if(reaction()){
      discardDownTo(3)
    }
  }

  def WitchAttack(): Unit ={
    if(reaction()){
      gainCard(Curse())
    }
  }

  def playAcard(options:ListBuffer[Card],text:String,forced:Boolean): Option[Card] ={
    options.foreach(x => println(x.detail))
    val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
    options.find(x => x.getName == name)
  }

  def playAcard(options:List[Card],text:String,forced:Boolean): Option[Card] ={
    options.foreach(x => println(x.detail))
    val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
    options.find(x => x.getName == name)
  }

  def LibraryAction(): Unit ={
    var aside = ListBuffer[Card]()
    while(deck.nonEmpty && hand.length < 7){
      drawXcards(1)
      if(hand.last.isAction){
        println("you can set "+ hand.last.getName +" aside instead, discarding it afterwards, type its NAME if you want to")
        val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
        val optCard = hand.find(x => x.getName == name)
        if(optCard.isDefined){
          hand -= optCard.get
          discardPile += optCard.get
        }
      }
    }
    discardPile ++= aside
  }

  def MineAction()={
    if(hand.exists(_.isTreasure)){
      hand.filter(_.isTreasure).foreach(x=> println(x))
      println("you may trash any card from these to gain a treasure costing up to 3 more than it")
      val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
      val optCard = hand.find(x => x.getName == name)
      if(optCard.isDefined){
        trash += optCard.get
        hand -= optCard.get
        session.board.filter(x => x._2>0 && x._1.isTreasure && x._1.getPrice<= optCard.get.getPrice+3).foreach(x=> println(x))
        println("name which one you gain")
        val toGain = scala.io.StdIn.readLine() // INFO bod rozhodnuti
        val optCardToGain = session.board.filter(x => x._2>0 && x._1.isTreasure && x._1.getPrice<= optCard.get.getPrice+3).find(x => x._1.getName == toGain)
        if(optCardToGain.isDefined){
          hand += optCard.get
          session.board(optCardToGain.get._1) = session.board(optCardToGain.get._1) -1
        }
      }
    }
  }

  def SentryAction()={
    drawXcards(1)
    actions += 1
    val list = drawXcardsIntoTemp(2)
    list.foreach({ card =>
      println("you can TRASH or DISCARD the following card? Otherwise put it back on top of your deck: "+card.getName)
      val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
      name match{
        case "TRASH"=> trash += card
        case "DISCARD"=> discardPile += card
        case _ => card +: deck
      }
    })

  }

  def ArtisanAction(): Unit ={
    gainCardToHandUpTo(5)
    println("put a card from your hand onto your deck. Name the card.")
    hand.foreach(x => println(x))
    val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
    val optCard = hand.find(x => x.getName == name)
    if(optCard.isDefined){
      deck += optCard.get
      hand -= optCard.get
    }
  }

  def ThroneRoomAction(): Unit ={
    // todo you may play an action card from your hand twice
    if(hand.exists(_.isAction)){
      println("you may play an action card from your hand twice")
      hand.filter(_.isAction).foreach(x => println(x))
      val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
      val optCard = hand.filter(_.isAction).find(x => x.getName == name)
      if(optCard.isDefined){
        hand -= optCard.get
        played += optCard.get
        for(a <- 1 to 2){
          optCard.get.Action(this)
        }
      }
    }
  }

  def getSession():Session={
    session
  }

  def shuffleDeck: Unit ={
    deck = util.Random.shuffle(deck)
  }

  def discardIntoDeck: Unit ={
    deck ++= discardPile
    discardPile.clear()
  }

  def drawXcards(x:Int): Unit ={
    for(y <- 1 to x){
      if(deck.isEmpty){
        discardIntoDeck
        shuffleDeck
        if(deck.isEmpty) return // no more cards to draw, every card is in play
      }
      println(name +" draws "+deck.head.getName)
      hand += deck.head
      deck -= deck.head
    }
  }

  def drawXcardsIntoTemp(x:Int) ={
    var list = ListBuffer[Card]()
    for(y <- 1 to x){
      if(deck.isEmpty){
        discardIntoDeck
        shuffleDeck
      }
      if(deck.nonEmpty){
        list += deck.head
        deck -= deck.head
      }
    }
    list
  }

  def victoryPoints:Int={
    var buf = ListBuffer[Card]()
    buf ++= deck
    buf ++= discardPile
    buf ++= hand
    buf ++= played
    buf.map(_.getVP(this)).sum
  }

  def gainCard(card:Card): Unit ={
    if(session.board(card)>0){
      println("gains "+card)
      card +=: discardPile// += card
      session.board(card) = session.board(card) -1
    }
  }

  def buyCard(card:Card) ={
    money -= card.getPrice
    buys -= 1
    gainCard(card)
  }

  def useAsAction(card:Card): Unit ={
    actions -= 1
    played += card
    hand -= card
    card.Action(this)
  }

  def useAsTreasure(card:Card): Unit ={
    money += card.getGain
    played += card
    hand -= card
  }

  def mayPlayAsAction(card:Card): Boolean ={
    println("you may play " + card + " type its name if you want to")
    val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
    if(card.getName == name){
      card.Action(this)
      true
    }else{
      false
    }
  }

  def trashOneFromHand(): Option[Card] ={
    println("trash a card from your hand to gain a card costing up to 2 mode than it")
    hand.foreach(x => println(x))
    val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
    val optCard = hand.find(x => x.getName == name)
    if(optCard.isDefined){
      hand -= optCard.get
      trash += optCard.get
    }
    optCard
  }

  def mayTrashFromHand(card:Card): Boolean ={
    if(hand.exists(p=> p.getName==card.getName)){
      println("you may trash a/an "+card.getName+" from your hand for +3")
      hand.foreach(x => println(x))
      val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
      val optCard = hand.find(x => x.getName == name)
      if(optCard.isDefined){
        trash += optCard.get
        hand -= optCard.get
        return true
      }
    }
    false
  }

  def gainCardUpTo(value:Int): Unit ={
    println("gain a card costing up to "+value)
    session.board.filter(c => c._1.getPrice <= value && c._2 >0).foreach(x => println(x))

    val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
    val optCard = session.board.find(x => x._1.getName == name)
    if(optCard.isDefined){
      discardPile += optCard.get._1
      session.board(optCard.get._1) = session.board(optCard.get._1)-1
    }

  }

  def gainCardToHandUpTo(value:Int): Unit ={
    println("gain a card costing up to "+value+" to your hand")
    session.board.filter(c => c._1.getPrice <= value && c._2 >0).foreach(x => println(x))

    val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
    val optCard = session.board.find(x => x._1.getName == name)
    if(optCard.isDefined){
      hand += optCard.get._1
      session.board(optCard.get._1) = session.board(optCard.get._1)-1
    }
  }

  def discard(number:Int)={
    for(x <- 1 to number){
      if(hand.nonEmpty){
        println("discard a card")
        val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
        val optCard = hand.find(x => x.getName == name)
        if(optCard.isDefined){
          hand -= optCard.get
          discardPile += optCard.get
        }
      }
    }
  }

  def discardAnyNumber():Int={
    var x = 0
    while(hand.nonEmpty){
      hand.foreach(x => println(x))
      val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
      val optCard = hand.find(x => x.getName == name)
      if(optCard.isDefined){
        x += 1
        hand -= optCard.get
        discardPile += optCard.get
      }else{
        return x
      }
    }
    x
  }

  def discardDownTo(number:Int): Unit ={
    println("discard a card")
    while(hand.length>number){
      hand.foreach(x => println(x))
      val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
      val optCard = hand.find(x => x.getName == name)
      if(optCard.isDefined){
        hand -= optCard.get
        discardPile += optCard.get
      }
    }
  }

  def discardTopDeck(): Card ={
      if(deck.isEmpty){
        discardIntoDeck
        shuffleDeck
        if(deck.isEmpty) return new Card("",0) // no more cards to draw, every card is in play
      }
      val card = deck.head
      discardPile += card
      deck -= card
    card
  }

  def trashHandUpTo(number:Int): Int ={
    var k = 0
    for(x <- 1 to number){
      hand.foreach(x => println(x))
      val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
      val optCard = hand.find(x => x.getName == name)
      if(optCard.isDefined){
        k += 1
        hand -= optCard.get
        trash += optCard.get
      }else{
        return k
      }
    }
    k
  }

  def lookAndMayPlaceXcardsFromDiscardPileontoDeck(x:Int): Unit ={
    for(x <- 1 to x){
      println("***you may put a card onto your deck")
      discardPile.foreach(x => println(x))
      val name = scala.io.StdIn.readLine() // INFO bod rozhodnuti
      val optCard = discardPile.find(x => x.getName == name)
      if(optCard.isDefined){
        optCard.get +=: deck
        discardPile -= optCard.get
      }else{
        return
      }
    }
  }


}
