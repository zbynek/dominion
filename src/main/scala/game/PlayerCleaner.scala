package game

import cards.{Discard, Trash}
import cards.types.Card

import scala.Option
import scala.collection.mutable.ListBuffer

class PlayerCleaner(name:String,_deck:ListBuffer[Card], session: Session) extends Player(name,_deck,session) {

  override def reaction():Boolean={
    if(hand.exists(_.isReaction)){
      val optCard = decide(hand.filter(_.isReaction),"you may use any of these reaction cards",false)
      if(optCard.isDefined){
        optCard.get.Reaction
        return false
      }
    }
    true
  }

  override  def BureucratAttack()={
    if(reaction()){
      if(hand.exists(_.isVictory)){
        val optCard = decide(hand.filter(_.isVictory),"reveal victory card and put it onto deck",true)
        if(optCard.isDefined){
          deck += optCard.get
          hand -= optCard.get
        }
      }else{
        // todo tohle predelat jakoby na show pro druhyho hrace
        hand.foreach(x => println(x.getName))
      }
    }
  }

  override def BanditAttack(): Unit ={
    if(reaction()){
      var reveal = ListBuffer[Card]()
      for(y <- 1 to 2){
        if(deck.isEmpty){
          discardIntoDeck
          shuffleDeck
        }
        if(deck.nonEmpty){
          reveal += deck.head
          deck -= deck.head
        }
      }
      if(reveal.exists(x => x.isTreasure && x.getName!="Copper")){
        val optCard = decide(reveal.filter(x => x.isTreasure && x.getName!="Copper"),"trash a revealed treasure other than copper and discard the rest",true)
        if(optCard.isDefined){
          reveal -= optCard.get
          trash += optCard.get
        }
        discardPile ++= reveal
      }
    }
  }

  override def ArtisanAction(): Unit ={
    gainCardToHandUpTo(5)
    val optCard = decide(hand,"put a card from your hand onto your deck.",true)
    if(optCard.isDefined){
      deck += optCard.get
      hand -= optCard.get
    }
  }

  override def LibraryAction(): Unit ={
    var aside = ListBuffer[Card]()
    while(deck.nonEmpty && hand.length < 7){
      drawXcards(1)
      if(hand.last.isAction){
        val optCard = decide(hand.last,"you can set card aside instead, discarding it afterwards",false)
        if(optCard.isDefined){
          hand -= optCard.get
          discardPile += optCard.get
        }
      }
    }
    discardPile ++= aside
  }

  override def MineAction()={
    if(hand.exists(_.isTreasure)){
      val optCard = decide(hand.filter(_.isTreasure),"you may trash a card to gain a treasure costing up to 3 more than it",false)
      if(optCard.isDefined){
        trash += optCard.get
        hand -= optCard.get
        val optCardToGain = decide(session.board.filter(x => x._2>0 && x._1.isTreasure && x._1.getPrice<= optCard.get.getPrice+3).keys.toList,"which one you want to gain?",true)
        if(optCardToGain.isDefined){
          hand += optCard.get
          session.board(optCardToGain.get) = session.board(optCardToGain.get) -1
        }
      }
    }
  }

  override def SentryAction()={
    drawXcards(1)
    actions += 1
    val list = drawXcardsIntoTemp(2)
    list.foreach({ card =>
      val name =decide(List(Trash(),Discard()),"you can TRASH or DISCARD the following card? Otherwise put it back on top of your deck: "+card.getName,false)
      name match{
        case Some(Trash()) => trash += card
        case Some(Discard())=> discardPile += card
        case None => card +: deck
      }
    })

  }

  override def ThroneRoomAction(): Unit ={

    if(hand.exists(_.isAction)){
      val optCard = decide(hand.filter(_.isAction),"you may play an action card from your hand twice",false)
      if(optCard.isDefined){
        hand -= optCard.get
        played += optCard.get
        for(a <- 1 to 2){
          optCard.get.Action(this)
        }
      }
    }
  }

  override def playAcard(options:ListBuffer[Card],text:String,forced:Boolean): Option[Card] ={
    decide(options,text,forced)
  }

  override def playAcard(options:List[Card],text:String,forced:Boolean): Option[Card] ={
   decide(options,text,forced)
  }

  def decide(options:List[Card],text:String,forced:Boolean):Option[Card]={
    while(true){
      println("HAND> "+hand.map(_.getName+" ").mkString)
      println(text)
      if(!forced)println("(0) do nothing")
      for(x <- 1 to options.length){
        println("("+x+") "+options(x-1).getName)
      }
      try {
        val index = scala.io.StdIn.readInt()
        index match {
          case 0 => if(forced){println("Not a valid option")}else return None
          case index if 1 until (options.length+1) contains index => return Option(options(index-1))
          case _ => println("Not a valid option")
        }
      }catch {
        case e:Exception => println("Not a number")
      }
    }
    None
  }

  def decide(options:Card,text:String,forced:Boolean):Option[Card]={
    decide(List(options),text,forced)
  }

  def decide(options:ListBuffer[Card],text:String,forced:Boolean):Option[Card]={
    decide(options.toList,text,forced)
  }

  override def mayPlayAsAction(card:Card): Boolean ={
    val card1 = card
    decide(card,"you may plan a card",false) match {
      case card1 =>  card.Action(this)
                            true
      case _=>              false
    }
  }

  override def trashOneFromHand(): Option[Card] ={
    if(hand.nonEmpty){
      val optCard = decide(hand,text="trash a card from your hand to gain a card costing up to 2 mode than it",forced = true)
      if(optCard.isDefined){
        hand -= optCard.get
        trash += optCard.get
      }
      optCard
    }else{None}
  }

  override def mayTrashFromHand(card:Card): Boolean ={
    if(hand.exists(p=> p.getName==card.getName)){
      val optCard =decide(hand,"you may trash a card from your hand for +3",false)
      if(optCard.isDefined){
        trash += optCard.get
        hand -= optCard.get
        return true
      }
    }
    false
  }

  override def gainCardUpTo(value:Int): Unit ={
    if(session.board.exists(c => c._1.getPrice <= value && c._2 >0)){
      val optCard = decide(session.board.filter(c => c._1.getPrice <= value && c._2 >0).keys.toList,text="gain a card costing up to "+value,forced = true)
      if(optCard.isDefined){
        discardPile += optCard.get
        session.board(optCard.get) = session.board(optCard.get)-1
      }
    }
  }

  override def gainCardToHandUpTo(value:Int): Unit ={
    if(session.board.exists(c => c._1.getPrice <= value && c._2 >0)){
      val optCard = decide(session.board.filter(c => c._1.getPrice <= value && c._2 >0).keys.toList,"gain a card costing up to "+value+" to your hand",true)
      if(optCard.isDefined){
        hand += optCard.get
        session.board(optCard.get) = session.board(optCard.get)-1
      }
    }
  }

  override def discard(number:Int)={
    for(x <- 1 to number){
      if(hand.nonEmpty){
        val optCard = decide(hand,"discard a card",forced = true)
        if(optCard.isDefined){
          hand -= optCard.get
          discardPile += optCard.get
        }
      }
    }
  }

  override def discardAnyNumber():Int={
    var x = 0
    while(hand.nonEmpty){
      val optCard = decide(hand,"You may discard any number of cards",false)
      if(optCard.isDefined){
        x += 1
        hand -= optCard.get
        discardPile += optCard.get
      }else{
        return x
      }
    }
    x
  }

  override def discardDownTo(number:Int): Unit ={
    println("discard a card")
    while(hand.length>number){
      val optCard = decide(hand,"discard down to "+number,true)
      if(optCard.isDefined){
        hand -= optCard.get
        discardPile += optCard.get
      }
    }
  }

  override def trashHandUpTo(number:Int): Int ={
    var k = 0
    for(x <- 1 to number){
      val optCard = decide(hand,"you may trash a card (4 in total)",false)
      if(optCard.isDefined){
        k += 1
        hand -= optCard.get
        trash += optCard.get
      }else{
        return k
      }
    }
    k
  }

  override def lookAndMayPlaceXcardsFromDiscardPileontoDeck(x:Int): Unit ={
    for(x <- 1 to x){
      val optCard = decide(discardPile,"you may put a card from your discard pile onto your deck",false)
      if(optCard.isDefined){
        optCard.get +=: deck
        discardPile -= optCard.get
      }else{
        return
      }
    }
  }
}
