package game

import cards._
import cards.types.Card
import scala.util.control.Breaks._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer


class Session() {
  var board = mutable.Map[Card,Int]()
  var players = new ListBuffer[Player]()


  def play: Unit ={
      do{
        players.head.turn += 1
        println(" NOW ITS "+players.head.getName()+" TURN ("+players.head.turn+")")
        println("ACTIONS \n")
        breakable{
          while(players.head.actions>0 && players.head.hand.exists(_.isAction)){
            val optCard = players.head.playAcard(players.head.hand.filter(_.isAction),"you may plan an action",false)
            if(optCard.isDefined){
              players.head.useAsAction(optCard.get)
              endTheGameCheck()
            }else{
              endTheGameCheck()
              break()
            }
          }
        }

        println("TREASURES \n")
        breakable {
          while(players.head.hand.exists(_.isTreasure)){
            val optCard = players.head.playAcard(players.head.hand.filter(_.isTreasure),"you may play a treasure",false)
            if(optCard.isDefined){
              players.head.effects.foreach({ e =>
                if(e(optCard.get)){
                  players.head.effects -= e // todo zkontrolovat chovani, jestli to odebira pri behu
                }
              })
              players.head.useAsTreasure(optCard.get)
              endTheGameCheck()
            }else{
              endTheGameCheck()
              println("nevybrana karta")
              break()
            }
          }
        }

        println("BUYS / money "+players.head.money+" \n")
        breakable{
          while(players.head.buys>0){
            val optCard = players.head.playAcard(board.filter(p => p._2 > 0 && p._1.getPrice <= players.head.money).keys.toList,"you may buy a card",false)
            if(optCard.isDefined){
              players.head.buyCard(optCard.get)
              endTheGameCheck()
            }else{
              endTheGameCheck()
              break()
            }
          }
        }
        players.head.cleanUp()

        players = players.tail += players.head
      }while(true)

  }


  def setUpTheGame(numberOfPlayers:Int,cards:List[Card]): Unit ={
//setup for 2 players
    cards.filter(_.isBasic).map({
      x => board += (x -> (x match{
        case c:Copper => 46
        case s:Silver => 40
        case g:Gold => 30
        case u:Curse => 10
        case _ => 8

      }))
    })

    util.Random.shuffle(cards.filterNot(_.isBasic)).take(10).map({
      x => board += (x -> (x match {
        case g:Gardens => 8
        case _ => 10
      }))
    })

    for(x <- 1 to numberOfPlayers){
      var deck = ListBuffer[Card](Copper(),Copper(),Copper(),Copper(),Copper(),Copper(),Estate(),Estate(),Estate())
      if(x==0){
        players += new BotVerze1("Player"+x.toString,deck,this)
      }else{
        players += new BotVerze1("Player"+x.toString,deck,this)
      }

    }

    players.map({ player =>
      player.shuffleDeck
      player.drawXcards(5)
      player.buys = 1
      player.money = 0
      player.actions = 1
    })

    println(board.groupBy(_._1).map(x=> x._1 + " " +x._2).mkString(", "))

  }

  def emptyPile():Int={
    board.count(c => c._2 == 0)
  }

  def endTheGameCheck(){
    if(board.exists(c=> c._1.isGameEnding && c._2<=0) || board.count(c=> c._2<=0)>=3){

      println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
      println("XXXXXXXXXX konec hry XXXXXXXX")
      println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX")

      players.sortBy(_.victoryPoints).foreach(x=> println(x.getName() + " VP: " + x.victoryPoints + " on turn: "+x.turn + " CARDS:  "+x.getAllCards().groupBy(_.getName).map(x=> x._1 + " " +x._2.size).mkString(", ")))

      board.foreach(x => println(x._1.getName + ": " +x._2))

      players.foreach(p => println("TRASH> "+p.trash.mkString(", ")))
      System.exit(0)

    }
  }



}
