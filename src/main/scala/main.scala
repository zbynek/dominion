import cards.types.Card
import game.Session

object main {
  def main(args: Array[String]): Unit = {

    val gameSession = new Session
    gameSession.setUpTheGame(2,Card.getAllCards)
    gameSession.play

  }

}
